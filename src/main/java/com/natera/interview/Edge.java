package com.natera.interview;

import java.util.Objects;

public class Edge<T> {
    private final T src;
    private final T dst;

    public Edge(T src, T dst) {
        this.src = src;
        this.dst = dst;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge<?> edge = (Edge<?>) o;
        return src.equals(edge.src) &&
                dst.equals(edge.dst);
    }

    @Override
    public int hashCode() {
        return Objects.hash(src, dst);
    }

    @Override
    public String toString() {
        return "(" + src + ")--(" + dst + ")";
    }
}
