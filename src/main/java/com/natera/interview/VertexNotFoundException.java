package com.natera.interview;

import com.natera.interview.graph.Graph;

public class VertexNotFoundException extends RuntimeException {
    private final Object vertex;
    private final Graph<?> graph;

    public VertexNotFoundException(Object vertex, Graph<?> graph) {
        super("Vertex " + vertex.toString() + " was not added to graph");
        this.vertex = vertex;
        this.graph = graph;
    }

    public Object getVertex() {
        return vertex;
    }

    public Graph<?> getGraph() {
        return graph;
    }
}
