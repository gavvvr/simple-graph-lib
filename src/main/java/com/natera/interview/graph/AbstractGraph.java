package com.natera.interview.graph;

import com.natera.interview.Edge;
import com.natera.interview.VertexNotFoundException;

import java.util.*;

abstract class AbstractGraph<T> implements Graph<T> {

    private final HashMap<T, Set<T>> adjacencies = new HashMap<>();

    @Override
    public boolean addVertex(T vertex) {
        if (adjacencies.containsKey(vertex)) {
            return false;
        }
        return adjacencies.put(vertex, null) == null;
    }

    @Override
    public boolean addEdge(T src, T dst) {
        ensureVertexOnGraph(src);
        ensureVertexOnGraph(dst);
        return abstractAddEdge(src, dst);
    }

    abstract boolean abstractAddEdge(T src, T dst);

    protected boolean doAddEdge(T src, T dst) {
        Set<T> destinations = adjacencies.computeIfAbsent(src, t -> new HashSet<>());
        return destinations.add(dst);
    }

    @Override
    public List<Edge<T>> getPath(T start, T end) {
        ensureVertexOnGraph(start);
        ensureVertexOnGraph(end);

        return dfsFindPath(start, end);
    }

    private List<Edge<T>> dfsFindPath(T start, T end) {
        Set<T> linkedVertices = adjacencies.get(start);
        if (linkedVertices == null || linkedVertices.size() == 0) {
            return Collections.emptyList();
        }
        Set<T> visited = new HashSet<>();
        visited.add(start);


        LinkedList<T> verticesPath = new LinkedList<>();
        verticesPath.add(start);

        Stack<T> stack = new Stack<>();
        stack.addAll(linkedVertices);
        while (!stack.isEmpty()) {
            T currentVertex = stack.peek();
            if (visited.contains(currentVertex)) {
                stack.pop();
                if (Objects.equals(currentVertex, verticesPath.getLast())) {
                    verticesPath.removeLast();
                }
                continue;
            }
            visited.add(currentVertex);
            verticesPath.add(currentVertex);

            if (Objects.equals(currentVertex, end)) {
                return buildPathFromVertices(verticesPath);
            }
            var moreVertices = adjacencies.get(currentVertex);
            if (moreVertices != null) {
                stack.addAll(moreVertices);
            }
        }
        return Collections.emptyList();
    }

    private List<Edge<T>> buildPathFromVertices(List<T> verticesPath) {
        List<Edge<T>> edgesPath = new ArrayList<>(verticesPath.size() - 1);
        T previous = verticesPath.get(0);
        for (T currentVertex : verticesPath.subList(1, verticesPath.size())) {
            edgesPath.add(new Edge<>(previous, currentVertex));
            previous = currentVertex;
        }
        return edgesPath;
    }

    private void ensureVertexOnGraph(T vertex) {
        if (!adjacencies.containsKey(vertex)) {
            throw new VertexNotFoundException(vertex, this);
        }
    }

}

