package com.natera.interview.graph;

import com.natera.interview.Edge;
import com.natera.interview.VertexNotFoundException;

import java.util.List;

/**
 * Simple graph
 *
 * @param <T> user-defined type of vertices on the graph
 */
public interface Graph<T> {

    /**
     * adds vertex to the graph
     *
     * @param vertex vertex to be added on the graph
     * @return {@code true} if vertex was added, {@code false} if already existed
     */
    boolean addVertex(T vertex);

    /**
     * Creates an edge between provided vertices
     *
     * @param src first vertex of the edge
     * @param dst second vertex of the edge
     * @return {@code true} if edge was added, {@code false} if already existed
     * @throws VertexNotFoundException if any of vertices was not added to graph
     */
    boolean addEdge(T src, T dst);

    /**
     * Finds a path between 2 vertices (the path is not supposed to be optimal)
     *
     * @param start starting vertex
     * @param end   destination vertex
     * @return a {@code list} of edges connecting 2 vertices, or empty list if there is no path
     * @throws VertexNotFoundException if any of vertices was not added to graph
     */
    List<Edge<T>> getPath(T start, T end);
}
