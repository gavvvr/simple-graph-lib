package com.natera.interview.graph;

class DirectedGraph<T> extends AbstractGraph<T> {

    @Override
    boolean abstractAddEdge(T src, T dst) {
        return doAddEdge(src, dst);
    }
}
