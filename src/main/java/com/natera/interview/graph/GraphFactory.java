package com.natera.interview.graph;

public class GraphFactory {
    public static <T> Graph<T> createDirectedGraph() {
        return new DirectedGraph<>();
    }

    public static <T> Graph<T> createUndirectedGraph() {
        return new UndirectedGraph<>();
    }


}
