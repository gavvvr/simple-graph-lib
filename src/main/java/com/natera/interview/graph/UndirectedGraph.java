package com.natera.interview.graph;

class UndirectedGraph<T> extends AbstractGraph<T> {
    @Override
    boolean abstractAddEdge(T src, T dst) {
        return doAddEdge(src, dst) && doAddEdge(dst, src);
    }
}
