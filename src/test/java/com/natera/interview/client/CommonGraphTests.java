package com.natera.interview.client;

import com.natera.interview.Edge;
import com.natera.interview.graph.Graph;
import com.natera.interview.graph.GraphFactory;
import com.natera.interview.VertexNotFoundException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CommonGraphTests {

    @AllGraphsTest
    void nullVertexIsAllowed(Graph<String> graph) {
        graph.addVertex(null);
    }

    @AllGraphsTest
    void emptyPathBetweenSameVertices(Graph<String> graph) {
        graph.addVertex(null);
        var path = graph.getPath(null, null);
        assertThat(path).isEmpty();
    }

    @AllGraphsTest
    void nonEmptyPathWithNullVertex(Graph<String> graph) {
        graph.addVertex("v1");
        graph.addVertex(null);
        graph.addVertex("v3");

        graph.addEdge("v1", null);
        graph.addEdge(null, "v3");

        var path = graph.getPath("v1", "v3");

        assertThat(path).hasSize(2);
    }

    @AllGraphsTest
    void nonEmptyPathWithNullStart(Graph<String> graph) {
        graph.addVertex(null);
        graph.addVertex("v2");
        graph.addVertex("v3");

        graph.addEdge(null, "v2");
        graph.addEdge("v2", "v3");

        var path = graph.getPath(null, "v3");

        assertThat(path).hasSize(2);
    }

    @AllGraphsTest
    void nonEmptyPathWithNullEnd(Graph<String> graph) {
        graph.addVertex("v1");
        graph.addVertex("v2");
        graph.addVertex(null);

        graph.addEdge("v1", "v2");
        graph.addEdge("v2", null);

        var path = graph.getPath("v1", null);

        assertThat(path).hasSize(2);
    }

    @AllGraphsTest
    void sameVertexCanNotBeAddedTwice(Graph<String> graph) {
        var vertex = "v";

        assertThat(graph.addVertex(vertex)).isTrue();
        assertThat(graph.addVertex(vertex)).isFalse();
    }

    @AllGraphsTest
    void sameEdgeCanNotBeAddedTwice(Graph<String> graph) {
        graph.addVertex("v1");
        graph.addVertex("v2");

        assertThat(graph.addEdge("v1", "v2")).isTrue();
        assertThat(graph.addEdge("v1", "v2")).isFalse();
    }

    @AllGraphsTest
    void canNotCreateEdgeBetweenNotExistingVertices(Graph<String> graph) {
        graph.addVertex("v2");

        var exception = assertThrows(VertexNotFoundException.class, () -> graph.addEdge("v1", "v2"));

        assertThat(exception.getVertex()).isEqualTo("v1");
        assertThat(exception.getGraph()).isSameAs(graph);
    }

    @AllGraphsTest
    void canNotEvaluatePathBetweenNotExistingVertices(Graph<String> graph) {
        var exception = assertThrows(VertexNotFoundException.class, () -> graph.getPath("v1", "v2"));

        assertThat(exception.getVertex()).isEqualTo("v1");
        assertThat(exception.getMessage()).isEqualTo("Vertex v1 was not added to graph");
    }

    @AllGraphsTest
    void notConnectedVerticesReturnEmptyPath(Graph<String> graph) {
        graph.addVertex("v1");
        graph.addVertex("v2");

        var path = graph.getPath("v1", "v2");

        assertThat(path).isEmpty();
    }

    @AllGraphsTest
    void cyclicGraphReturnsPath(Graph<String> graph) {
        graph.addVertex("v1");
        graph.addVertex("v2");
        graph.addVertex("v3");
        graph.addVertex("v4");

        graph.addEdge("v1", "v2");
        graph.addEdge("v2", "v3");
        graph.addEdge("v3", "v1");
        graph.addEdge("v1", "v4");

        var path = graph.getPath("v1", "v4");
        assertThat(path).isNotEmpty();
    }

    static Stream<Graph<String>> graphProvider() {
        return Stream.of(
                GraphFactory.createUndirectedGraph(),
                GraphFactory.createDirectedGraph()
        );
    }
}

@ParameterizedTest
@MethodSource("graphProvider")
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface AllGraphsTest {
}
