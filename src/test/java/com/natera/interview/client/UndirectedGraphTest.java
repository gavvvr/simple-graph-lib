package com.natera.interview.client;

import com.natera.interview.Edge;
import com.natera.interview.graph.Graph;
import com.natera.interview.graph.GraphFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UndirectedGraphTest {

    Graph<String> graph;

    @BeforeEach
    void prepare() {
        graph = GraphFactory.createUndirectedGraph();
    }

    @Test
    void testUndirectedGraphPath() {
        graph.addVertex("v1");
        graph.addVertex("v2");
        graph.addVertex("v3");
        graph.addVertex("v4");
        graph.addVertex("v5");
        graph.addVertex("v6");
        graph.addVertex("v7");

        graph.addEdge("v4", "v3");
        graph.addEdge("v3", "v5");
        graph.addEdge("v5", "v7");
        graph.addEdge("v3", "v1");
        graph.addEdge("v1", "v6");
        graph.addEdge("v1", "v2");

        var path = graph.getPath("v4", "v2");

        assertThat(path).containsExactly(
                new Edge<>("v4", "v3"),
                new Edge<>("v3", "v1"),
                new Edge<>("v1", "v2")
        );
    }

    @Test
    void undirectedGraphHasOppositePath() {
        graph.addVertex("v1");
        graph.addVertex("v2");
        graph.addVertex("v3");
        graph.addEdge("v1", "v2");
        graph.addEdge("v2", "v3");

        var path = graph.getPath("v3", "v1");

        assertThat(path).containsExactly(
                new Edge<>("v3", "v2"),
                new Edge<>("v2", "v1")
        );
    }
}
