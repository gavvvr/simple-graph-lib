package com.natera.interview.client;

import com.natera.interview.Edge;
import com.natera.interview.graph.Graph;
import com.natera.interview.graph.GraphFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DirectedGraphTest {

    Graph<String> graph;

    @BeforeEach
    void prepare() {
        graph = GraphFactory.createDirectedGraph();
    }

    @Test
    void testDirectedGraphPath() {
        graph.addVertex("v1");
        graph.addVertex("v2");
        graph.addVertex("v3");
        graph.addVertex("v4");

        graph.addEdge("v2", "v1");
        graph.addEdge("v1", "v3");
        graph.addEdge("v3", "v4");
        graph.addEdge("v4", "v2");

        var path = graph.getPath("v1", "v2");

        assertThat(path).containsExactly(
                new Edge<>("v1", "v3"),
                new Edge<>("v3", "v4"),
                new Edge<>("v4", "v2")
        );
    }

    @Test
    void directedGraphDoesNotHaveOppositePath() {
        graph.addVertex("v1");
        graph.addVertex("v2");
        graph.addVertex("v3");

        graph.addEdge("v1", "v2");
        graph.addEdge("v2", "v3");

        var path = graph.getPath("v3", "v1");
        assertThat(path).isEmpty();
    }

}
